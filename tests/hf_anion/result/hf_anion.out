


                     eT 1.5 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.5.0 Furia (development)
  ------------------------------------------------------------
  Configuration date: 2021-10-06 10:20:02 UTC +02:00
  Git branch:         hf-cleanup-oao-stuff
  Git hash:           82fed81e1022a04236e45d2edec97a412b5fa7bd
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-10-06 10:32:29 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: Cl-
        charge: -1
        multiplicity: 1
     end system

     do
        ground state
     end do

     method
        hf
     end method

     memory
        available: 8
     end memory

     solver scf
       algorithm:          scf-diis
       energy threshold:   1.0d-10
       gradient threshold: 1.0d-10
     end solver scf


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1 Cl     0.000000000000     0.000000000000     0.000000000000        1
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1 Cl     0.000000000000     0.000000000000     0.000000000000        1
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               13
     Number of orthonormal atomic orbitals:   13

  - Molecular orbital details:

     Number of occupied orbitals:         9
     Number of virtual orbitals:          4
     Number of molecular orbitals:       13


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - Setting initial AO density to sad

  Warning: mismatch in number of electrons from initial guess. It can 
           be beneficial to assign charges to atoms.

     Energy of initial guess:              -457.074117301345
     Number of electrons in guess:           17.000000000000
     Overall charge:                                      -1

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -457.320710253880     0.8118E-01     0.4573E+03
     2          -457.350696079980     0.2347E-01     0.2999E-01
     3          -457.353583381418     0.6954E-03     0.2887E-02
     4          -457.353585322902     0.1735E-03     0.1941E-05
     5          -457.353585381269     0.3155E-05     0.5837E-07
     6          -457.353585381287     0.3055E-07     0.1853E-10
     7          -457.353585381288     0.9919E-09     0.3411E-12
     8          -457.353585381288     0.5131E-12     0.1137E-12
  ---------------------------------------------------------------
  Convergence criterion met in 8 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  1.097920135568
     Nuclear repulsion energy:       0.000000000000
     Electronic energy:           -457.353585381288
     Total energy:                -457.353585381288

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.16500
     Total cpu time (sec):               0.26749

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 44.016 KB

  :: There was 1 warning during the execution of eT. ::

  Total wall time in eT (sec):              0.17900
  Total cpu time in eT (sec):               0.28074

  Calculation ended: 2021-10-06 10:32:30 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
