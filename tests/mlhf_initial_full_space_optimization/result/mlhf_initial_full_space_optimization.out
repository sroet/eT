


                     eT 1.5 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.5.0 Furia (development)
  ------------------------------------------------------------
  Configuration date: 2021-10-06 10:20:02 UTC +02:00
  Git branch:         hf-cleanup-oao-stuff
  Git hash:           82fed81e1022a04236e45d2edec97a412b5fa7bd
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-10-06 10:32:00 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: 2 H2O close
        charge: 0
        multiplicity: 1
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm: mo-scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
        coulomb threshold:  1.0d-14
        exchange threshold: 1.0d-14
     end solver scf

     method
        mlhf
     end method

     multilevel hf
        initial hf optimization
        initial hf threshold: 1.0d-3
     end multilevel hf

     active atoms
        selection type: list
        hf: {1, 2, 3}
     end active atoms


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: MLHF wavefunction
  =======================

  - MLHF settings:

     Occupied orbitals:    Cholesky
     Virtual orbitals:     PAOs

     Cholesky decomposition threshold:  0.10E-01

     Initial optimization of full AO density enabled

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  O    -0.573030000000     2.189950000000    -0.052560000000        1
        2  H     0.347690000000     2.485980000000     0.050490000000        2
        3  H    -1.075800000000     3.019470000000     0.020240000000        3
        4  O    -1.567030000000    -0.324500000000     0.450780000000        4
        5  H    -1.211220000000     0.588750000000     0.375890000000        5
        6  H    -1.604140000000    -0.590960000000    -0.479690000000        6
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  O    -1.082869761160     4.138405726491    -0.099324005107        1
        2  H     0.657038876250     4.697821351146     0.095412272029        2
        3  H    -2.032967364807     5.705971341340     0.038248056761        3
        4  O    -2.961257528977    -0.613216127421     0.851850742431        4
        5  H    -2.288874076596     1.112576255838     0.710329152963        5
        6  H    -3.031385265460    -1.116752550573    -0.906482724693        6
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               48
     Number of orthonormal atomic orbitals:   48

  - Molecular orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         38
     Number of molecular orbitals:       48


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a MLHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (MO-SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (MO-SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - Setting initial AO density to sad

     Energy of initial guess:              -151.796506244373
     Number of electrons in guess:           20.000000000000

  - Initial full hf optimization to a gradient threshold of  0.10E-02

  - Active orbital space:

      Number of active occupied orbitals:        5
      Number of active virtual orbitals:        23
      Number of active orbitals:                28

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-13
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-13
     One-electron integral  cutoff:  0.1000E-18
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -152.058301564710     0.1155E-02     0.1521E+03
     2          -152.058302192275     0.4069E-03     0.6276E-06
     3          -152.058302306403     0.7494E-04     0.1141E-06
     4          -152.058302307904     0.1886E-04     0.1500E-08
     5          -152.058302308077     0.4225E-05     0.1728E-09
     6          -152.058302308086     0.9786E-06     0.8811E-11
     7          -152.058302308086     0.1118E-06     0.5969E-12
     8          -152.058302308086     0.3255E-07     0.1137E-12
     9          -152.058302308086     0.7923E-08     0.5684E-13
    10          -152.058302308086     0.2454E-08     0.2842E-13
    11          -152.058302308086     0.8599E-10     0.8527E-13
    12          -152.058302308086     0.1929E-10     0.2842E-13
    13          -152.058302308086     0.8158E-11     0.5684E-13
  ---------------------------------------------------------------
  Convergence criterion met in 13 iterations!

  - Summary of MLHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.692362627223
     Nuclear repulsion energy:      37.386395233393
     Electronic energy:           -189.444697541480
     Total energy:                -152.058302308086

  - Summary of MLHF active/inactive contributions to electronic energy (a.u.):

     Active energy:               -104.799512917235
     Active-inactive energy:        19.316446319109
     Inactive energy:             -103.961630943354

  - Timings for the MLHF ground state calculation

     Total wall time (sec):              3.35800
     Total cpu time (sec):               6.35603

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 685.152 KB

  Total wall time in eT (sec):              3.39200
  Total cpu time in eT (sec):               6.39264

  Calculation ended: 2021-10-06 10:32:04 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
