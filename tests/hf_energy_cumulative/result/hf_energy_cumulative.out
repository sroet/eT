


                     eT 1.5 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.5.0 Furia (development)
  ------------------------------------------------------------
  Configuration date: 2021-10-06 10:20:02 UTC +02:00
  Git branch:         hf-cleanup-oao-stuff
  Git hash:           82fed81e1022a04236e45d2edec97a412b5fa7bd
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-10-06 10:32:15 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF He
        charge: 0
     end system

     print
        output print level: verbose
     end print

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:                 scf-diis
        energy threshold:          1.0d-10
        gradient threshold:        1.0d-10
        cumulative fock threshold: 1.0d-3
     end solver scf

     method
        hf
     end method


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

  Libint electron repulsion integral precision:  0.1000E-23

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  F    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.000000000000     0.000000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  F    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.000000000000     0.000000000000    14.172945934238        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               38
     Number of orthonormal atomic orbitals:   38

  - Molecular orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         28
     Number of molecular orbitals:       38


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density
     Generated atomic density for H  using UHF/cc-pvdz
     Generated atomic density for F  using UHF/cc-pvdz
     Generated atomic density for O  using UHF/cc-pvdz
     Generated atomic density for He using UHF/cc-pvdz


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - Setting initial AO density to sad

     Energy of initial guess:              -178.316362504469
     Number of electrons in guess:           20.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E-02

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -177.452442672057     0.7565E-01     0.1775E+03
     2          -177.479038695768     0.1129E-01     0.2660E-01
     3          -177.480033265333     0.4145E-02     0.9946E-03
     4          -177.480160935731     0.1388E-02     0.1277E-03
     5          -177.480174929178     0.4311E-03     0.1399E-04
  Fock matrix construction using density differences
     6          -177.480176282849     0.8270E-04     0.1354E-05
  Fock matrix construction using density differences
     7          -177.480176366015     0.2291E-04     0.8317E-07
  Fock matrix construction using density differences
     8          -177.480176376348     0.4810E-05     0.1033E-07
  Fock matrix construction using density differences
     9          -177.480176377178     0.1046E-05     0.8306E-09
  Fock matrix construction using density differences
    10          -177.480176377209     0.2394E-06     0.3092E-10
  Fock matrix construction using density differences
    11          -177.480176377210     0.5253E-07     0.7674E-12
  Fock matrix construction using density differences
    12          -177.480176377210     0.9777E-08     0.8527E-13
  Fock matrix construction using density differences
    13          -177.480176377210     0.2792E-08     0.1421E-12
  Fock matrix construction using density differences
    14          -177.480176377210     0.1441E-08     0.1421E-12
  Fock matrix construction using density differences
    15          -177.480176377210     0.6293E-09     0.8527E-13
  Fock matrix construction using density differences
    16          -177.480176377210     0.1436E-09     0.1137E-12
  Fock matrix construction using density differences
    17          -177.480176377210     0.4658E-10     0.0000E+00
  ---------------------------------------------------------------
  Convergence criterion met in 17 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.600136348427
     Nuclear repulsion energy:      48.518317619727
     Electronic energy:           -225.998493996937
     Total energy:                -177.480176377210

  - Timings for the RHF ground state calculation

     Total wall time (sec):              2.65200
     Total cpu time (sec):               4.91889

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 307.628 KB

  Total wall time in eT (sec):              2.69500
  Total cpu time in eT (sec):               4.96365

  Calculation ended: 2021-10-06 10:32:18 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
