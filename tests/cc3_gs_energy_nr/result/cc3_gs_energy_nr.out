


                     eT 1.5 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.5.0 Furia (development)
  ------------------------------------------------------------
  Configuration date: 2021-10-06 10:20:02 UTC +02:00
  Git branch:         hf-cleanup-oao-stuff
  Git hash:           82fed81e1022a04236e45d2edec97a412b5fa7bd
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-10-06 10:29:57 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     method
       hf
       cc3
     end method

     print
        output print level: verbose
     end print

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     solver cc gs
        multimodel newton: off
        algorithm: newton-raphson
        energy threshold: 1.0d-11
        omega threshold:  1.0d-11
     end solver cc gs

     do
        ground state
     end do


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

  Libint electron repulsion integral precision:  0.1000E-23

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density
     Generated atomic density for H  using UHF/cc-pvdz
     Generated atomic density for O  using UHF/cc-pvdz
     Generated atomic density for He using UHF/cc-pvdz


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492022836321
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592585     0.9786E-01     0.7880E+02
  Fock matrix construction using density differences
     2           -78.828675852654     0.7077E-01     0.3207E-01
  Fock matrix construction using density differences
     3           -78.843487343819     0.6747E-02     0.1481E-01
  Fock matrix construction using density differences
     4           -78.843814479549     0.2753E-02     0.3271E-03
  Fock matrix construction using density differences
     5           -78.843850612079     0.3973E-03     0.3613E-04
  Fock matrix construction using density differences
     6           -78.843851670925     0.5220E-04     0.1059E-05
  Fock matrix construction using density differences
     7           -78.843851692779     0.6096E-05     0.2185E-07
  Fock matrix construction using density differences
     8           -78.843851693528     0.2137E-05     0.7488E-09
  Fock matrix construction using density differences
     9           -78.843851693630     0.3151E-06     0.1027E-09
  Fock matrix construction using density differences
    10           -78.843851693631     0.2637E-07     0.5116E-12
  Fock matrix construction using density differences
    11           -78.843851693631     0.5634E-08     0.2842E-13
  Fock matrix construction using density differences
    12           -78.843851693631     0.1415E-08     0.8527E-13
  Fock matrix construction using density differences
    13           -78.843851693631     0.6806E-09     0.1421E-13
  Fock matrix construction using density differences
    14           -78.843851693631     0.1491E-09     0.7105E-13
  Fock matrix construction using density differences
    15           -78.843851693631     0.2501E-10     0.0000E+00
  Fock matrix construction using density differences
    16           -78.843851693631     0.5166E-11     0.5684E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080245
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.89100
     Total cpu time (sec):               1.54415

  No frozen fock contributions!

  Libint electron repulsion integral precision:  0.1000E-23


  :: CC3 wavefunction
  ======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Ground state coupled cluster engine
  =========================================

  Calculates the ground state CC wavefunction | CC > = exp(T) | R >

  This is a CC3 ground state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (newton-raphson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals
  Doing Cholesky decomposition of the ERIs.

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

  - Preparing diagonal for decomposition:

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.38100
     Total cpu time (sec):               0.59497


  2) Preparation of MO basis and integrals
  No MO preparations for cc3


  3) Calculation of the ground state (newton-raphson algorithm)

   - Davidson tool settings:

     Number of parameters:                 9729
     Number of requested solutions:           1
     Max reduced space dimension:            50

     Storage (newton_raphson_amplitude_updator_trials): file
     Storage (newton_raphson_amplitude_updator_transforms): file

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_errors): file
     Storage (cc_gs_diis_parameters): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931608     0.9358E-01     0.7908E+02

  Preparing for cc3 right excited state equations

     Iteration     Residual norm
     -----------------------------
       1           0.2323E-01
       2           0.5910E-02
       3           0.1640E-02
       4           0.5758E-03
     -----------------------------

    2           -79.104321650749     0.6497E-03     0.2008E-01

  Preparing for cc3 right excited state equations

     Iteration     Residual norm
     -----------------------------
       1           0.1843E-03
       2           0.4238E-04
       3           0.1345E-04
       4           0.6342E-05
     -----------------------------

    3           -79.104388570210     0.6474E-05     0.6692E-04

  Preparing for cc3 right excited state equations

     Iteration     Residual norm
     -----------------------------
       1           0.2575E-05
       2           0.1337E-05
       3           0.3661E-06
       4           0.1380E-06
       5           0.6206E-07
     -----------------------------

    4           -79.104388524003     0.6903E-07     0.4621E-07

  Preparing for cc3 right excited state equations

     Iteration     Residual norm
     -----------------------------
       1           0.2555E-07
       2           0.1405E-07
       3           0.2274E-08
       4           0.1015E-08
       5           0.4368E-09
     -----------------------------

    5           -79.104388532992     0.6077E-09     0.8989E-08

  Preparing for cc3 right excited state equations

     Iteration     Residual norm
     -----------------------------
       1           0.2836E-09
       2           0.1353E-09
       3           0.5395E-10
       4           0.1921E-10
       5           0.4351E-11
     -----------------------------

    6           -79.104388533033     0.5948E-11     0.4071E-10

  Preparing for cc3 right excited state equations

     Iteration     Residual norm
     -----------------------------
       1           0.2631E-11
       2           0.1303E-11
       3           0.4264E-12
       4           0.2021E-12
       5           0.5344E-13
     -----------------------------

    7           -79.104388533033     0.4239E-13     0.1421E-12
  ---------------------------------------------------------------
  Convergence criterion met in 7 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.104388533033

     Correlation energy (a.u.):           -0.260536839402

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.015274304435
       14      4       -0.008970023658
        7      4        0.007312583244
        4      5        0.006872684673
        2      4        0.006012921703
       15      5       -0.005704150040
        6      2        0.005025576014
       13      5        0.004933889031
        5      6        0.004366074635
        3      4        0.004126391914
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.049931914504
        5      6       5      6       -0.046567925291
        9      3       9      3       -0.041349512985
        3      4       3      4       -0.037778342513
        1      5       1      5       -0.037299297230
        6      5       6      5       -0.034903296771
       16      3      16      3       -0.032107572471
       17      3      17      3       -0.032051843284
        2      4       1      5       -0.031565942819
       18      3      18      3       -0.031349888894
     --------------------------------------------------

  - Finished solving the CC3 ground state equations

     Total wall time (sec):              6.40400
     Total cpu time (sec):               6.72150

  - Timings for the CC3 ground state calculation

     Total wall time (sec):              6.78600
     Total cpu time (sec):               7.31789

  - Cleaning up cc3 wavefunction

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 17.780568 MB

  Total wall time in eT (sec):              7.72400
  Total cpu time in eT (sec):               8.90941

  Calculation ended: 2021-10-06 10:30:05 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     CC3: https://doi.org/10.1021/acs.jctc.0c00686
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!
