


                     eT 1.5 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   M. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.5.0 Furia (development)
  ------------------------------------------------------------
  Configuration date: 2021-12-13 17:02:21 UTC +01:00
  Git branch:         input-cleanup
  Git hash:           4c2cc6bf523455801cdf275544d28013b6da142b
  Fortran compiler:   GNU 9.3.0
  C compiler:         GNU 9.3.0
  C++ compiler:       GNU 9.3.0
  LAPACK type:        MKL
  BLAS type:          MKL
  64-bit integers:    ON
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-12-13 17:04:00 UTC +01:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF He
        charge: 0
     end system

     do
        time dependent hf
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-12
        gradient threshold: 1.0d-12
     end solver scf

     method
        hf
     end method

     solver tdhf
       tamm-dancoff
       states: 3
       residual threshold: 1.0d-10
     end solver tdhf


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.000000000000     0.000000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.000000000000     0.000000000000    14.172945934238        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Time dependent Hartree-Fock engine
  ========================================

  Drives the calculation of the Hartree-Fock excitation energies and properties.

  This is a RHF properties calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)
     3) Calculate TDHF excitation energies


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492359869020
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-17
     Exchange screening threshold:   0.1000E-15
     ERI cutoff:                     0.1000E-17
     One-electron integral  cutoff:  0.1000E-22
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-11
     Energy threshold:              0.1000E-11

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796357979403     0.9791E-01     0.7880E+02
     2           -78.828453237454     0.7078E-01     0.3210E-01
     3           -78.843281517241     0.6747E-02     0.1483E-01
     4           -78.843608975052     0.2753E-02     0.3275E-03
     5           -78.843645123311     0.3974E-03     0.3615E-04
     6           -78.843646182586     0.5223E-04     0.1059E-05
     7           -78.843646204409     0.6084E-05     0.2182E-07
     8           -78.843646205153     0.2132E-05     0.7440E-09
     9           -78.843646205256     0.3153E-06     0.1024E-09
    10           -78.843646205256     0.2623E-07     0.4974E-12
    11           -78.843646205256     0.5559E-08     0.4263E-13
    12           -78.843646205256     0.1724E-08     0.7105E-13
    13           -78.843646205256     0.7589E-09     0.0000E+00
    14           -78.843646205256     0.1286E-09     0.1421E-13
    15           -78.843646205256     0.2324E-10     0.2842E-13
    16           -78.843646205256     0.4592E-11     0.0000E+00
    17           -78.843646205256     0.1327E-11     0.1421E-13
    18           -78.843646205256     0.1393E-12     0.2842E-13
  ---------------------------------------------------------------
  Convergence criterion met in 18 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645693538853
     Nuclear repulsion energy:      12.167611407170
     Electronic energy:            -91.011257612426
     Total energy:                 -78.843646205256


  3) Calculate TDHF excitation energies

   - Davidson tool settings:

     Number of parameters:                  138
     Number of requested solutions:           3
     Max reduced space dimension:            50

     Storage (tdhf_davidson_trials): memory
     Storage (tdhf_davidson_transforms): memory

  Tamm-Dancoff approximations (CIS/CCS) enabled!

  - Davidson solver settings

     Number of singlet states:               3
     Max number of iterations:             100

  Iteration:                  1
  Reduced space dimension:    3

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.304704140451    0.000000000000     0.1314E+00   0.3047E+00
     2   0.372677109151    0.000000000000     0.1472E+00   0.3727E+00
     3   0.412719505371    0.000000000000     0.1736E+00   0.4127E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    6

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283505623315    0.000000000000     0.2353E-01   0.2120E-01
     2   0.336714558434    0.000000000000     0.1620E-01   0.3596E-01
     3   0.386244550220    0.000000000000     0.5954E-01   0.2647E-01
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    9

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283170290481    0.000000000000     0.3710E-02   0.3353E-03
     2   0.336463893371    0.000000000000     0.3528E-02   0.2507E-03
     3   0.381924726777    0.000000000000     0.1433E-01   0.4320E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:   12

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283164488619    0.000000000000     0.3641E-03   0.5802E-05
     2   0.336458737693    0.000000000000     0.3747E-03   0.5156E-05
     3   0.381657141751    0.000000000000     0.4934E-02   0.2676E-03
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   15

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283164449557    0.000000000000     0.6823E-04   0.3906E-07
     2   0.336458513869    0.000000000000     0.4940E-03   0.2238E-06
     3   0.381644277969    0.000000000000     0.1487E-02   0.1286E-04
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   18

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283164448832    0.000000000000     0.7908E-05   0.7254E-09
     2   0.336458448276    0.000000000000     0.1107E-03   0.6559E-07
     3   0.381644072172    0.000000000000     0.1355E-03   0.2058E-06
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   21

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283164448824    0.000000000000     0.4438E-06   0.7533E-11
     2   0.336458445914    0.000000000000     0.4649E-04   0.2362E-08
     3   0.381644068318    0.000000000000     0.1311E-04   0.3854E-08
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   24

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283164448824    0.000000000000     0.2358E-07   0.4829E-13
     2   0.336458444888    0.000000000000     0.7247E-05   0.1026E-08
     3   0.381644068265    0.000000000000     0.1178E-05   0.5381E-10
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   27

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283164448824    0.000000000000     0.3731E-08   0.1055E-14
     2   0.336458444866    0.000000000000     0.9352E-06   0.2212E-10
     3   0.381644068264    0.000000000000     0.1530E-06   0.5179E-12
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   30

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283164448824    0.000000000000     0.8113E-09   0.1166E-14
     2   0.336458444866    0.000000000000     0.1379E-06   0.2218E-12
     3   0.381644068264    0.000000000000     0.2696E-07   0.5995E-14
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   33

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283164448824    0.000000000000     0.4116E-10   0.1721E-14
     2   0.336458444866    0.000000000000     0.1026E-07   0.2442E-14
     3   0.381644068264    0.000000000000     0.2756E-08   0.4330E-14
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   35

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283164448824    0.000000000000     0.4120E-10   0.1443E-14
     2   0.336458444866    0.000000000000     0.9638E-09   0.8882E-15
     3   0.381644068264    0.000000000000     0.1885E-09   0.1055E-14
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   37

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  ------------------------------------------------------------------------
     1   0.283164448824    0.000000000000     0.4102E-10   0.4219E-14
     2   0.336458444866    0.000000000000     0.6100E-10   0.2776E-15
     3   0.381644068264    0.000000000000     0.2440E-10   0.1277E-14
  ------------------------------------------------------------------------
  Convergence criterion met in 13 iterations!

  - TDHF excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.283164448824        7.705297124094
        2                  0.336458444866        9.155500622939
        3                  0.381644068264       10.385064063778
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Timings for the RHF properties calculation

     Total wall time (sec):              2.02476
     Total cpu time (sec):               3.99507

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 349.720 KB

  Total wall time in eT (sec):              2.14281
  Total cpu time in eT (sec):               4.05784

  Calculation ended: 2021-12-13 17:04:02 UTC +01:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
