


                     eT 1.4 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.4.0 Eos (development)
  ------------------------------------------------------------
  Configuration date: 2021-08-19 15:38:41 UTC +02:00
  Git branch:         RI
  Git hash:           b9d95c9d79ca2b792856de46c24642e86dc74164
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-08-19 16:01:40 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        hf
        ccsd
     end method

     solver cc gs
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     solver cc es
        algorithm:          davidson
        singlet states:     2
        residual threshold: 1.0d-10
        energy threshold:   1.0d-10
        right eigenvectors
     end solver cc es

     integrals
        ri: cc-pVDZ
     end integrals


  Running on 4 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492022836338
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592630     0.9053E-01     0.7880E+02
     2           -78.828675852673     0.7128E-01     0.3207E-01
     3           -78.843428154462     0.8290E-02     0.1475E-01
     4           -78.843809541675     0.2414E-02     0.3814E-03
     5           -78.843850846799     0.3301E-03     0.4131E-04
     6           -78.843851669326     0.5527E-04     0.8225E-06
     7           -78.843851692851     0.5865E-05     0.2353E-07
     8           -78.843851693557     0.2356E-05     0.7054E-09
     9           -78.843851693630     0.2071E-06     0.7375E-10
    10           -78.843851693631     0.1594E-07     0.4405E-12
    11           -78.843851693631     0.3322E-08     0.2842E-13
    12           -78.843851693631     0.1197E-08     0.0000E+00
    13           -78.843851693631     0.4264E-09     0.1421E-13
    14           -78.843851693631     0.1280E-09     0.2842E-13
    15           -78.843851693631     0.1479E-10     0.1421E-13
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080236
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.37300
     Total cpu time (sec):               1.10044


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  138
     Double excitation amplitudes:  9591


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CCSD excited state calculation.
  The following tasks will be performed:

     1) RI approximation of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) RI approximation of the electron repulsion integrals

  - Summary of RI approximation of electronic repulsion integrals:

     Auxilliary basis set: cc-pvdz
     Dimension of auxilliary basis: 29

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    True


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_errors): file
     Storage (cc_gs_diis_parameters): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.081826253750     0.1966E+00     0.7908E+02
    2           -79.077186359211     0.3471E-01     0.4640E-02
    3           -79.081931752239     0.1016E-01     0.4745E-02
    4           -79.083071584689     0.2842E-02     0.1140E-02
    5           -79.083229518837     0.7033E-03     0.1579E-03
    6           -79.083247595940     0.2321E-03     0.1808E-04
    7           -79.083219053051     0.5565E-04     0.2854E-04
    8           -79.083227941539     0.1242E-04     0.8888E-05
    9           -79.083227047795     0.3699E-05     0.8937E-06
   10           -79.083227095737     0.1665E-05     0.4794E-07
   11           -79.083227021547     0.7358E-06     0.7419E-07
   12           -79.083227003187     0.2791E-06     0.1836E-07
   13           -79.083227025985     0.8760E-07     0.2280E-07
   14           -79.083227028821     0.2528E-07     0.2836E-08
   15           -79.083227028308     0.5405E-08     0.5126E-09
   16           -79.083227028469     0.1440E-08     0.1609E-09
   17           -79.083227028456     0.5151E-09     0.1360E-10
   18           -79.083227028473     0.1336E-09     0.1772E-10
   19           -79.083227028481     0.4394E-10     0.8015E-11
  ---------------------------------------------------------------
  Convergence criterion met in 19 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.083227028481

     Correlation energy (a.u.):           -0.239375334851

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        5      6        0.052166296471
        6      5       -0.027683950362
        4      5        0.019911550919
        1      5        0.019899895685
        7      4       -0.016268658064
       10      5       -0.012991350294
        1      2       -0.012752811254
       11      6        0.012106910189
        9      6        0.012065967938
        3      4        0.011188098344
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        2      4       2      4       -0.041942893173
        5      6       5      6       -0.036238698096
        3      4       3      4       -0.033102211343
        1      5       1      5       -0.033008632106
        9      3       9      3       -0.031934130579
       16      3      16      3       -0.030471277127
       17      3      17      3       -0.030423176824
       18      3      18      3       -0.029833698269
        2      4       1      5       -0.028086691334
        2      4       3      4       -0.027286149561
     --------------------------------------------------

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.16800
     Total cpu time (sec):               0.37168


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue problem 
  is solved in a reduced space, the dimension of which is expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

     Number of singlet states:               2
     Max number of iterations:             100

   - Davidson tool settings:

     Number of parameters:                 9729
     Number of requested solutions:           2
     Max reduced space dimension:           100

     Storage (cc_es_davidson_trials): file
     Storage (cc_es_davidson_transforms): file

  Iteration:                  1
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.360303269284    0.000000000000     0.4184E+00   0.3603E+00
     2   0.479104888095    0.000000000000     0.4128E+00   0.4791E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.257796172402    0.000000000000     0.8451E-01   0.1025E+00
     2   0.379407308227    0.000000000000     0.1056E+00   0.9970E-01
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.254882754604    0.000000000000     0.3215E-01   0.2913E-02
     2   0.371451640247    0.000000000000     0.3964E-01   0.7956E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255020675914    0.000000000000     0.7648E-02   0.1379E-03
     2   0.371516295343    0.000000000000     0.1530E-01   0.6466E-04
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255076705934    0.000000000000     0.1658E-02   0.5603E-04
     2   0.371365575639    0.000000000000     0.4408E-02   0.1507E-03
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078850734    0.000000000000     0.4556E-03   0.2145E-05
     2   0.371364461644    0.000000000000     0.2028E-02   0.1114E-05
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078455585    0.000000000000     0.1509E-03   0.3951E-06
     2   0.371336964236    0.000000000000     0.7068E-03   0.2750E-04
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078442348    0.000000000000     0.5267E-04   0.1324E-07
     2   0.371337317918    0.000000000000     0.3107E-03   0.3537E-06
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078470351    0.000000000000     0.4402E-04   0.2800E-07
     2   0.371325523559    0.000000000000     0.1534E-02   0.1179E-04
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078480811    0.000000000000     0.2299E-04   0.1046E-07
     2   0.325918791117    0.000000000000     0.1190E+00   0.4541E-01
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   22

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078495356    0.000000000000     0.6686E-05   0.1454E-07
     2   0.317899826385    0.000000000000     0.3199E-01   0.8019E-02
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078498869    0.000000000000     0.2059E-05   0.3514E-08
     2   0.318095366931    0.000000000000     0.1019E-01   0.1955E-03
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   26

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078497183    0.000000000000     0.7529E-06   0.1686E-08
     2   0.318075814102    0.000000000000     0.4366E-02   0.1955E-04
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496066    0.000000000000     0.3385E-06   0.1117E-08
     2   0.318059658816    0.000000000000     0.2564E-02   0.1616E-04
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   30

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496014    0.000000000000     0.1573E-06   0.5160E-10
     2   0.318046559177    0.000000000000     0.1392E-02   0.1310E-04
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496265    0.000000000000     0.7267E-07   0.2505E-09
     2   0.318038034023    0.000000000000     0.7392E-03   0.8525E-05
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   34

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496342    0.000000000000     0.2475E-07   0.7685E-10
     2   0.318041596757    0.000000000000     0.2945E-03   0.3563E-05
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496357    0.000000000000     0.7889E-08   0.1530E-10
     2   0.318040611051    0.000000000000     0.1085E-03   0.9857E-06
  ------------------------------------------------------------------------

  Iteration:                 19
  Reduced space dimension:   38

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496342    0.000000000000     0.2382E-08   0.1474E-10
     2   0.318040438081    0.000000000000     0.3596E-04   0.1730E-06
  ------------------------------------------------------------------------

  Iteration:                 20
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496339    0.000000000000     0.6660E-09   0.3679E-11
     2   0.318040537997    0.000000000000     0.1048E-04   0.9992E-07
  ------------------------------------------------------------------------

  Iteration:                 21
  Reduced space dimension:   42

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496342    0.000000000000     0.1576E-09   0.2926E-11
     2   0.318040543422    0.000000000000     0.2589E-05   0.5426E-08
  ------------------------------------------------------------------------

  Iteration:                 22
  Reduced space dimension:   44

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496343    0.000000000000     0.3954E-10   0.1437E-11
     2   0.318040541715    0.000000000000     0.6895E-06   0.1707E-08
  ------------------------------------------------------------------------

  Iteration:                 23
  Reduced space dimension:   45

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496344    0.000000000000     0.1295E-10   0.4693E-12
     2   0.318040538096    0.000000000000     0.2163E-06   0.3619E-08
  ------------------------------------------------------------------------

  Iteration:                 24
  Reduced space dimension:   46

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496344    0.000000000000     0.8546E-11   0.1060E-12
     2   0.318040537741    0.000000000000     0.6105E-07   0.3544E-09
  ------------------------------------------------------------------------

  Iteration:                 25
  Reduced space dimension:   47

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496344    0.000000000000     0.8319E-11   0.1282E-13
     2   0.318040537991    0.000000000000     0.1507E-07   0.2494E-09
  ------------------------------------------------------------------------

  Iteration:                 26
  Reduced space dimension:   48

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496344    0.000000000000     0.8307E-11   0.2776E-14
     2   0.318040537972    0.000000000000     0.4193E-08   0.1824E-10
  ------------------------------------------------------------------------

  Iteration:                 27
  Reduced space dimension:   49

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496344    0.000000000000     0.8313E-11   0.2942E-14
     2   0.318040537958    0.000000000000     0.1391E-08   0.1424E-10
  ------------------------------------------------------------------------

  Iteration:                 28
  Reduced space dimension:   50

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496344    0.000000000000     0.8292E-11   0.7216E-15
     2   0.318040537965    0.000000000000     0.4217E-09   0.7073E-11
  ------------------------------------------------------------------------

  Iteration:                 29
  Reduced space dimension:   51

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496344    0.000000000000     0.8282E-11   0.5551E-16
     2   0.318040537968    0.000000000000     0.1275E-09   0.2531E-11
  ------------------------------------------------------------------------

  Iteration:                 30
  Reduced space dimension:   52

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.255078496344    0.000000000000     0.8289E-11   0.3220E-14
     2   0.318040537968    0.000000000000     0.3680E-10   0.5168E-12
  ------------------------------------------------------------------------
  Convergence criterion met in 30 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.255078496344
     Fraction singles (|R1|/|R|):       0.976398997531

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      6        0.967449581233
        4      6        0.125663071380
       13      6       -0.022243304815
        6      6       -0.018243457579
       10      6        0.014807350395
        1      3       -0.011520308934
        1      5        0.008589029663
       19      5       -0.008385760779
       20      4       -0.007753024315
        8      4       -0.007103605857
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       1      6       -0.095859418535
        1      5       1      6       -0.084468592475
        1      4       2      6       -0.057795241185
        1      6       5      6       -0.054949988683
        1      2       1      6       -0.054553049343
        3      4       1      6       -0.053425650610
        6      5       1      6        0.042227110931
        4      5       1      6       -0.038742848991
        4      4       2      6       -0.034347276436
        2      5       2      6       -0.031974533561
     --------------------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.318040537968
     Fraction singles (|R1|/|R|):       0.976715573143

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        2      6        0.960506426540
        3      6        0.171856593705
        7      6       -0.039998981904
        2      3       -0.011533483242
        2      5        0.006275082688
        8      2       -0.005267372025
       23      6       -0.004078927787
        3      3       -0.003511430731
       11      4       -0.002898826914
       14      6        0.002785633416
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        2      4       2      6       -0.104005745782
        1      5       2      6       -0.080172909085
        2      5       1      6       -0.049005146871
        2      6       5      6       -0.047982131118
        1      4       1      6       -0.047566650438
        3      4       2      6       -0.047171628952
        1      2       2      6       -0.041027283140
        4      5       2      6       -0.039041654444
        4      4       1      6       -0.035959798945
        3      5       1      6       -0.035502563809
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.255078496344        6.941039429409
        2                  0.318040537968        8.654323848662
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (right)

     Total wall time (sec):              0.75900
     Total cpu time (sec):               1.41618

  - Timings for the CCSD excited state calculation

     Total wall time (sec):              0.95400
     Total cpu time (sec):               1.82345

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 10.734168 MB

  Total wall time in eT (sec):              1.38100
  Total cpu time in eT (sec):               2.97337

  Calculation ended: 2021-08-19 16:01:41 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
