


                     eT 1.5 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, M. Scavino, 
   A. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.5.0 Furia (development)
  ------------------------------------------------------------
  Configuration date: 2021-10-06 10:20:02 UTC +02:00
  Git branch:         hf-cleanup-oao-stuff
  Git hash:           82fed81e1022a04236e45d2edec97a412b5fa7bd
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2021-10-06 10:30:31 UTC +02:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
        excited state
     end do

     memory
        available: 8
     end memory

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver scf
        restart
        algorithm:          scf-diis
        energy threshold:   1.0d-12
        gradient threshold: 1.0d-12
     end solver scf

     method
        hf
        mlcc2
     end method

     solver cc gs
        restart
        omega threshold:  1.0d-10
        energy threshold: 1.0d-10
     end solver cc gs

     solver cc es
        restart
        algorithm:          davidson
        singlet states:     4
        residual threshold: 1.0d-10
        energy threshold:   1.0d-10
        right eigenvectors
     end solver cc es

     active atoms
        selection type: list
        cc2: {3}
     end active atoms

     mlcc
        cc2 orbitals: cholesky
        cholesky threshold: 1.0d-1
     end mlcc


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  O     0.000000000000    -0.075790000000     5.000000000000        3
        2  H     0.866810000000     0.601440000000     5.000000000000        1
        3  H    -0.866810000000     0.601440000000     5.000000000000        2
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  O     0.000000000000    -0.143222342981     9.448630622825        3
        2  H     1.638033502034     1.136556880358     9.448630622825        1
        3  H    -1.638033502034     1.136556880358     9.448630622825        2
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Calculation of reference state (SCF-DIIS algorithm)


  1) Calculation of reference state (SCF-DIIS algorithm)

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-11
     Energy threshold:              0.1000E-11

  - Requested restart. Reading orbitals from file

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-17
     Exchange screening threshold:   0.1000E-15
     ERI cutoff:                     0.1000E-17
     One-electron integral  cutoff:  0.1000E-22
     Cumulative Fock threshold:      0.1000E+01

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.843851693631     0.2368E-12     0.7884E+02
  ---------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080251
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.02500
     Total cpu time (sec):               0.04659


  :: MLCC2 wavefunction
  ========================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     23
     Molecular orbitals:   29
     Atomic orbitals:      29


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a MLCC2 excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               118
     Significant AO pairs:                  431

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               406 /     108       0.47383E+01         147             42             17052
     2               319 /      93       0.47165E-01         234            111             35409
     3               250 /      76       0.46944E-03         178            183             45750
     4               187 /      55       0.38270E-05         145            265             49555
     5                87 /      25       0.38106E-07          78            324             28188
     6                21 /       3       0.37202E-09          43            349              7329
     7                 0 /       0       0.36652E-11           7            354                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 354

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7161E-12
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False
     T1 ERI matrix in memory:    False

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.48000
     Total cpu time (sec):               0.63006


  2) Preparation of MO basis and integrals

     The smallest diagonal after decomposition is:  -0.1022E-16

     The smallest diagonal after decomposition is:  -0.1358E-13

  - MLCC2 orbital partitioning:

     Orbital type: cholesky

     Number occupied cc2 orbitals:    5
     Number virtual cc2 orbitals:    13

     Number occupied ccs orbitals:    1
     Number virtual ccs orbitals:    10


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09
     Max number of iterations:      100

  Requested restart. Reading in solution from file.

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_errors): file
     Storage (cc_gs_diis_parameters): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -78.992658321885     0.8201E-12     0.7899E+02
  ---------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -78.992658321885

     Correlation energy (a.u.):           -0.148806628254

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
       15      4       -0.016328878160
        5      3        0.014564153928
        6      4        0.012826074950
       17      3        0.012491270947
        3      3        0.009204805602
       14      3       -0.007937198481
        2      5        0.006832883845
       20      3       -0.003941989393
        1      2        0.003004112650
       15      2       -0.002970409243
     ------------------------------------

  - Finished solving the MLCC2 ground state equations

     Total wall time (sec):              0.01100
     Total cpu time (sec):               0.01142


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue problem 
  is solved in a reduced space, the dimension of which is expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

     Number of singlet states:               4
     Max number of iterations:             100

   - Davidson tool settings:

     Number of parameters:                 2283
     Number of requested solutions:           4
     Max reduced space dimension:           100

     Storage (cc_es_davidson_trials): file
     Storage (cc_es_davidson_transforms): file
     Restarting right vector 1 from file l_001.
     Restarting right vector 2 from file l_002.
     Restarting right vector 3 from file l_003.
     Restarting right vector 4 from file l_004.

  Iteration:                  1
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311542901402    0.000000000000     0.1317E-01   0.1041E-03
     2   0.364941666728    0.000000000000     0.1188E-01   0.1340E-03
     3   0.405941096891    0.000000000000     0.5892E-01   0.1251E-03
     4   0.457405630104    0.000000000000     0.1231E-01   0.9738E-04
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311434636104    0.000000000000     0.2562E-02   0.1083E-03
     2   0.364801525258    0.000000000000     0.2319E-02   0.1401E-03
     3   0.405819255680    0.000000000000     0.2240E-02   0.1218E-03
     4   0.457301557091    0.000000000000     0.2720E-02   0.1041E-03
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311437445074    0.000000000000     0.1217E-02   0.2809E-05
     2   0.364800025029    0.000000000000     0.1250E-02   0.1500E-05
     3   0.405816443266    0.000000000000     0.8135E-03   0.2812E-05
     4   0.457303397622    0.000000000000     0.1558E-02   0.1841E-05
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311439188229    0.000000000000     0.3008E-03   0.1743E-05
     2   0.364807799171    0.000000000000     0.1035E-03   0.7774E-05
     3   0.405815247129    0.000000000000     0.2959E-03   0.1196E-05
     4   0.457310637998    0.000000000000     0.7654E-03   0.7240E-05
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   20

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311439003448    0.000000000000     0.1021E-03   0.1848E-06
     2   0.364807601541    0.000000000000     0.1600E-04   0.1976E-06
     3   0.405816444125    0.000000000000     0.1365E-03   0.1197E-05
     4   0.457305751954    0.000000000000     0.1239E-02   0.4886E-05
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438768014    0.000000000000     0.2012E-04   0.2354E-06
     2   0.364807630234    0.000000000000     0.9792E-05   0.2869E-07
     3   0.405815899856    0.000000000000     0.5337E-04   0.5443E-06
     4   0.457308292735    0.000000000000     0.1421E-02   0.2541E-05
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   28

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438816261    0.000000000000     0.5054E-05   0.4825E-07
     2   0.364807626317    0.000000000000     0.3181E-05   0.3917E-08
     3   0.405815938071    0.000000000000     0.1864E-04   0.3822E-07
     4   0.457310740974    0.000000000000     0.1065E-02   0.2448E-05
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   32

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438821431    0.000000000000     0.1600E-05   0.5171E-08
     2   0.364807616874    0.000000000000     0.1004E-05   0.9443E-08
     3   0.405816011148    0.000000000000     0.1592E-04   0.7308E-07
     4   0.457308120411    0.000000000000     0.2737E-03   0.2621E-05
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   36

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820446    0.000000000000     0.5116E-06   0.9859E-09
     2   0.364807620086    0.000000000000     0.1959E-06   0.3212E-08
     3   0.405815995828    0.000000000000     0.8222E-05   0.1532E-07
     4   0.457308252075    0.000000000000     0.1315E-03   0.1317E-06
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   40

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820425    0.000000000000     0.1781E-06   0.2081E-10
     2   0.364807619983    0.000000000000     0.6796E-07   0.1033E-09
     3   0.405815975283    0.000000000000     0.3126E-05   0.2054E-07
     4   0.457308242847    0.000000000000     0.4466E-04   0.9229E-08
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   44

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820477    0.000000000000     0.4781E-07   0.5243E-10
     2   0.364807620028    0.000000000000     0.2267E-07   0.4570E-10
     3   0.405815979513    0.000000000000     0.1219E-05   0.4230E-08
     4   0.457308229749    0.000000000000     0.1206E-04   0.1310E-07
  ------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   48

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820478    0.000000000000     0.1035E-07   0.8339E-12
     2   0.364807620019    0.000000000000     0.4456E-08   0.9758E-11
     3   0.405815980596    0.000000000000     0.2770E-06   0.1083E-08
     4   0.457308242825    0.000000000000     0.1581E-05   0.1308E-07
  ------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   52

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820474    0.000000000000     0.2182E-08   0.4245E-11
     2   0.364807620015    0.000000000000     0.9305E-09   0.3620E-11
     3   0.405815979761    0.000000000000     0.5883E-07   0.8352E-09
     4   0.457308247621    0.000000000000     0.3142E-06   0.4796E-08
  ------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   56

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820474    0.000000000000     0.3576E-09   0.1756E-12
     2   0.364807620015    0.000000000000     0.1655E-09   0.4308E-12
     3   0.405815979813    0.000000000000     0.1067E-07   0.5192E-10
     4   0.457308246600    0.000000000000     0.7018E-07   0.1020E-08
  ------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   60

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820474    0.000000000000     0.4966E-10   0.1896E-12
     2   0.364807620015    0.000000000000     0.2471E-10   0.1971E-12
     3   0.405815979838    0.000000000000     0.1697E-08   0.2551E-10
     4   0.457308246754    0.000000000000     0.1235E-07   0.1538E-09
  ------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   62

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820474    0.000000000000     0.2128E-10   0.1871E-13
     2   0.364807620015    0.000000000000     0.6067E-11   0.1127E-13
     3   0.405815979832    0.000000000000     0.2810E-09   0.6683E-11
     4   0.457308246775    0.000000000000     0.2020E-08   0.2108E-10
  ------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   64

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820474    0.000000000000     0.1999E-10   0.8882E-15
     2   0.364807620015    0.000000000000     0.4956E-11   0.8327E-15
     3   0.405815979832    0.000000000000     0.3699E-10   0.7045E-12
     4   0.457308246773    0.000000000000     0.3281E-09   0.1978E-11
  ------------------------------------------------------------------------

  Iteration:                 18
  Reduced space dimension:   65

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.311438820474    0.000000000000     0.1998E-10   0.4996E-15
     2   0.364807620015    0.000000000000     0.4956E-11   0.5440E-14
     3   0.405815979832    0.000000000000     0.3613E-10   0.3886E-15
     4   0.457308246772    0.000000000000     0.4461E-10   0.5521E-12
  ------------------------------------------------------------------------
  Convergence criterion met in 18 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.311438820474
     Fraction singles (|R1|/|R|):       0.998500898646

     MLCC diagnostics:

     |R1^internal|/|R| =       0.556319394930
     |R1^internal|/|R1| =      0.557154626185

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
       15      5       -0.812868167352
        6      5        0.516170330334
        4      5        0.180237956221
       16      5       -0.162234469705
       12      5       -0.065368798025
        1      5        0.054735307894
        7      5        0.043283525687
        8      5        0.024658717930
        2      5        0.019694008118
       11      5       -0.014132968047
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.364807620015
     Fraction singles (|R1|/|R|):       0.997950547483

     MLCC diagnostics:

     |R1^internal|/|R| =       0.660097599964
     |R1^internal|/|R1| =      0.661453216924

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
       17      5        0.547208412301
        3      5        0.523393275766
       14      5       -0.503802434508
        5      5        0.400124089716
       20      5       -0.081570718825
       13      5        0.039917247008
       14      6        0.008440416703
        3      6       -0.006838474397
       17      6       -0.006543512101
        2      5        0.006371125442
     ------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.405815979832
     Fraction singles (|R1|/|R|):       0.998318120755

     MLCC diagnostics:

     |R1^internal|/|R| =       0.561879932188
     |R1^internal|/|R1| =      0.562826538462

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
       15      4        0.798832610849
        6      4       -0.499364807378
        4      4       -0.197740154273
       16      4        0.148449375700
       17      3        0.117055922700
        3      3        0.099743539336
        5      3        0.091722996918
       14      3       -0.077135260107
       12      4        0.052835634104
        2      5       -0.045798272360
     ------------------------------------

     Electronic state nr. 4

     Energy (Hartree):                  0.457308246772
     Fraction singles (|R1|/|R|):       0.998200691097

     MLCC diagnostics:

     |R1^internal|/|R| =       0.627728553610
     |R1^internal|/|R1| =      0.628860067127

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
       14      4        0.524937949092
       17      4       -0.496334167670
        3      4       -0.476062084508
        5      4       -0.355598561799
       15      3       -0.259240725972
        6      3        0.180075291591
       16      3       -0.083997747618
       20      4        0.075844096165
        4      3        0.065668145133
       13      4       -0.047168737713
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.311438820474        8.474681965525
        2                  0.364807620015        9.926920971273
        3                  0.405815979832       11.042815280303
        4                  0.457308246772       12.443991233055
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the MLCC2 excited state equations (right)

     Total wall time (sec):              0.64400
     Total cpu time (sec):               0.56268

  - Timings for the MLCC2 excited state calculation

     Total wall time (sec):              1.15100
     Total cpu time (sec):               1.22384

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 10.665712 MB

  Total wall time in eT (sec):              1.25700
  Total cpu time in eT (sec):               1.33230

  Calculation ended: 2021-10-06 10:30:33 UTC +02:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     MLCC2 and MLCCSD: https://doi.org/10.1021/acs.jctc.9b00701
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!
